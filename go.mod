module lambdadb

go 1.15

require (
	github.com/Attumm/settingo v1.5.0
	github.com/JensRantil/go-csv v0.0.0-20200923162218-7ffda755f61b
	github.com/Workiva/go-datastructures v1.0.52
	github.com/cheggaaa/pb v1.0.29
	github.com/go-spatial/geom v0.0.0-20200810200216-9bf4204b30e9
	github.com/goccy/go-json v0.10.2
	github.com/golang/geo v0.0.0-20230421003525-6adc56603217
	github.com/klauspost/compress v1.17.0 // indirect
	github.com/klauspost/pgzip v1.2.6
	github.com/kr/pretty v0.3.1 // indirect
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.8.0 // indirect
	golang.org/x/net v0.7.0
	golang.org/x/sys v0.8.0 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)
