/*
	Transforming ItemsIn -> Items -> ItemsOut
	Where Items has column values stored as integers to save memmory
	maps are needed to restore integers back to the actual string values.
	those are generated and stored here.
*/

package main

type ModelMaps struct {
	Provincie    MappedColumn
	Buurtcode    MappedColumn
	Wijkcode     MappedColumn
	Gemeentecode MappedColumn
}

var BitArrays map[string]fieldBitarrayMap

var Provincie MappedColumn
var Buurtcode MappedColumn
var Wijkcode MappedColumn
var Gemeentecode MappedColumn

func clearBitArrays() {
	BitArrays = make(map[string]fieldBitarrayMap)
}

func init() {
	clearBitArrays()
	setUpRepeatedColumns()
}

func setUpRepeatedColumns() {
	Provincie = NewReapeatedColumn("provincie")
	Buurtcode = NewReapeatedColumn("buurtcode")
	Wijkcode = NewReapeatedColumn("wijkcode")
	Gemeentecode = NewReapeatedColumn("gemeentecode")

}

func CreateMapstore() ModelMaps {
	return ModelMaps{
		Provincie,
		Buurtcode,
		Wijkcode,
		Gemeentecode,
	}
}

func LoadMapstore(m ModelMaps) {

	Provincie = m.Provincie
	Buurtcode = m.Buurtcode
	Wijkcode = m.Wijkcode
	Gemeentecode = m.Gemeentecode

	RegisteredColumns[Provincie.Name] = Provincie
	RegisteredColumns[Buurtcode.Name] = Buurtcode
	RegisteredColumns[Wijkcode.Name] = Wijkcode
	RegisteredColumns[Gemeentecode.Name] = Gemeentecode

}
