/*
	model.go define the 'items' to store.
	All columns with getters and setters are defined here.

	ItemIn, represent rows from the Input data
	Item, the compact item stored in memmory
	ItemOut, defines how and which fields are exported out
	of the API. It is possible to ignore input columns

	Repeated values are stored in maps with int numbers
	as keys.  Optionally bitarrays are created for reapeated
	column values to do fast bit-wise filtering.

	A S2 geo index in created for lat, lon values.

	Unique values are stored as-is.

	The generated codes leaves room to create custom
	index functions yourself to create an API with an
	< 1 ms response time for your specific needs.

        This codebase solves:

	The need to have an blazing fast API on
	a tabular dataset fast!
*/

package main

import (
	"encoding/json"
	"fmt"
	"sort"
	"strconv"
	"strings"

	"github.com/Workiva/go-datastructures/bitarray"
)

type registerGroupByFunc map[string]func(*Item) string
type registerGettersMap map[string]func(*Item) string
type registerReduce map[string]func(Items) map[string]string

/*
 * Options lists keys in registerReduce.
 * used for suggestions of debugging.
 */
func (r registerReduce) Options() string {
	options := []string{}
	for k := range r {
		options = append(options, k)
	}
	return strings.Join(options, ",")
}

/*
 * Options lists keys in registerGroupByFunc.
 *
 */

func (r registerGroupByFunc) Options() string {
	options := []string{}
	for k := range r {
		options = append(options, k)
	}
	return strings.Join(options, ",")
}

type registerBitArray map[string]func(s string) (bitarray.BitArray, error)
type fieldBitarrayMap map[uint32]bitarray.BitArray

type ItemIn struct {
	Nr                     string `json:"nr"`
	Provincie              string `json:"provincie"`
	Gemeente               string `json:"gemeente"`
	Wijk                   string `json:"wijk"`
	Buurt                  string `json:"buurt"`
	Regiocode              string `json:"regiocode"`
	SoortEigendomWoning    string `json:"soort_eigendom_woning"`
	AantalHuishoudens1     string `json:"aantal_huishoudens1"`
	EnergiearmoedeLihelek2 string `json:"energiearmoede_lihelek2"`
	EnergiearmoedeHeq      string `json:"energiearmoede_heq"`
	EnergiearmoedeLihe     string `json:"energiearmoede_lihe"`
	EnergiearmoedeLilek    string `json:"energiearmoede_lilek"`
	EnergiearmoedeLekwi    string `json:"energiearmoede_lekwi"`
	ResidueelInkomen       string `json:"residueel_inkomen"`
	Energiequote           string `json:"energiequote"`
	Id                     string `json:"id"`
	Buurtcode              string `json:"buurtcode"`
	Wijkcode               string `json:"wijkcode"`
	Gemeentecode           string `json:"gemeentecode"`
}

type ItemOut struct {
	Nr                     string `json:"nr"`
	Provincie              string `json:"provincie"`
	Gemeente               string `json:"gemeente"`
	Wijk                   string `json:"wijk"`
	Buurt                  string `json:"buurt"`
	Regiocode              string `json:"regiocode"`
	SoortEigendomWoning    string `json:"soort_eigendom_woning"`
	AantalHuishoudens1     string `json:"aantal_huishoudens1"`
	EnergiearmoedeLihelek2 string `json:"energiearmoede_lihelek2"`
	EnergiearmoedeHeq      string `json:"energiearmoede_heq"`
	EnergiearmoedeLihe     string `json:"energiearmoede_lihe"`
	EnergiearmoedeLilek    string `json:"energiearmoede_lilek"`
	EnergiearmoedeLekwi    string `json:"energiearmoede_lekwi"`
	ResidueelInkomen       string `json:"residueel_inkomen"`
	Energiequote           string `json:"energiequote"`
	Buurtcode              string `json:"buurtcode"`
	Wijkcode               string `json:"wijkcode"`
	Gemeentecode           string `json:"gemeentecode"`
}

type Item struct {
	Label                  int // internal index in ITEMS
	Nr                     string
	Provincie              uint32
	Gemeente               string
	Wijk                   string
	Buurt                  string
	Regiocode              string
	SoortEigendomWoning    string
	AantalHuishoudens1     string
	EnergiearmoedeLihelek2 string
	EnergiearmoedeHeq      string
	EnergiearmoedeLihe     string
	EnergiearmoedeLilek    string
	EnergiearmoedeLekwi    string
	ResidueelInkomen       string
	Energiequote           string
	Buurtcode              uint32
	Wijkcode               uint32
	Gemeentecode           uint32
}

func (i Item) MarshalJSON() ([]byte, error) {
	return json.Marshal(i.Serialize())
}

// Shrink create smaller Item using uint32
func (i ItemIn) Shrink(label int) Item {

	Provincie.Store(i.Provincie)
	Buurtcode.Store(i.Buurtcode)
	Wijkcode.Store(i.Wijkcode)
	Gemeentecode.Store(i.Gemeentecode)

	return Item{
		label,
		i.Nr,
		Provincie.GetIndex(i.Provincie),
		i.Gemeente,
		i.Wijk,
		i.Buurt,
		i.Regiocode,
		i.SoortEigendomWoning,
		i.AantalHuishoudens1,
		i.EnergiearmoedeLihelek2,
		i.EnergiearmoedeHeq,
		i.EnergiearmoedeLihe,
		i.EnergiearmoedeLilek,
		i.EnergiearmoedeLekwi,
		i.ResidueelInkomen,
		i.Energiequote,
		Buurtcode.GetIndex(i.Buurtcode),
		Wijkcode.GetIndex(i.Wijkcode),
		Gemeentecode.GetIndex(i.Gemeentecode),
	}
}

// Store selected columns in seperate map[columnvalue]bitarray
// for fast indexed selection
func (i *Item) StoreBitArrayColumns() {
	SetBitArray("buurtcode", i.Buurtcode, i.Label)
	SetBitArray("wijkcode", i.Wijkcode, i.Label)
	SetBitArray("gemeentecode", i.Gemeentecode, i.Label)

}

func (i Item) Serialize() ItemOut {
	return ItemOut{
		i.Nr,
		Provincie.GetValue(i.Provincie),
		i.Gemeente,
		i.Wijk,
		i.Buurt,
		i.Regiocode,
		i.SoortEigendomWoning,
		i.AantalHuishoudens1,
		i.EnergiearmoedeLihelek2,
		i.EnergiearmoedeHeq,
		i.EnergiearmoedeLihe,
		i.EnergiearmoedeLilek,
		i.EnergiearmoedeLekwi,
		i.ResidueelInkomen,
		i.Energiequote,
		Buurtcode.GetValue(i.Buurtcode),
		Wijkcode.GetValue(i.Wijkcode),
		Gemeentecode.GetValue(i.Gemeentecode),
	}
}

func (i ItemIn) Columns() []string {
	return []string{
		"nr",
		"provincie",
		"gemeente",
		"wijk",
		"buurt",
		"regiocode",
		"soort_eigendom_woning",
		"aantal_huishoudens1",
		"energiearmoede_lihelek2",
		"energiearmoede_heq",
		"energiearmoede_lihe",
		"energiearmoede_lilek",
		"energiearmoede_lekwi",
		"residueel_inkomen",
		"energiequote",
		"id",
		"buurtcode",
		"wijkcode",
		"gemeentecode",
	}
}

func (i ItemOut) Columns() []string {
	return []string{
		"nr",
		"provincie",
		"gemeente",
		"wijk",
		"buurt",
		"regiocode",
		"soort_eigendom_woning",
		"aantal_huishoudens1",
		"energiearmoede_lihelek2",
		"energiearmoede_heq",
		"energiearmoede_lihe",
		"energiearmoede_lilek",
		"energiearmoede_lekwi",
		"residueel_inkomen",
		"energiequote",
		"buurtcode",
		"wijkcode",
		"gemeentecode",
	}
}

func (i Item) Row() []string {

	return []string{
		i.Nr,
		Provincie.GetValue(i.Provincie),
		i.Gemeente,
		i.Wijk,
		i.Buurt,
		i.Regiocode,
		i.SoortEigendomWoning,
		i.AantalHuishoudens1,
		i.EnergiearmoedeLihelek2,
		i.EnergiearmoedeHeq,
		i.EnergiearmoedeLihe,
		i.EnergiearmoedeLilek,
		i.EnergiearmoedeLekwi,
		i.ResidueelInkomen,
		i.Energiequote,
		Buurtcode.GetValue(i.Buurtcode),
		Wijkcode.GetValue(i.Wijkcode),
		Gemeentecode.GetValue(i.Gemeentecode),
	}
}

func (i Item) GetIndex() string {
	return GettersNr(&i)
}

func (i Item) GetGeometry() string {
	return ""
}

// contain filter Nr
func FilterNrContains(i *Item, s string) bool {
	return strings.Contains(i.Nr, s)
}

// startswith filter Nr
func FilterNrStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Nr, s)
}

// match filters Nr
func FilterNrMatch(i *Item, s string) bool {
	return i.Nr == s
}

// gte filters Nr
func FilterNrgte(i *Item, s string) bool {
	return i.Nr <= s
}

// lte filters Nr
func FilterNrlte(i *Item, s string) bool {
	return i.Nr <= s
}

// getter Nr
func GettersNr(i *Item) string {
	return i.Nr
}

// contain filter Provincie
func FilterProvincieContains(i *Item, s string) bool {
	return strings.Contains(Provincie.GetValue(i.Provincie), s)
}

// startswith filter Provincie
func FilterProvincieStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Provincie.GetValue(i.Provincie), s)
}

// match filters Provincie
func FilterProvincieMatch(i *Item, s string) bool {
	return Provincie.GetValue(i.Provincie) == s
}

// gte filters Provincie
func FilterProvinciegte(i *Item, s string) bool {
	return Provincie.GetValue(i.Provincie) <= s
}

// lte filters Provincie
func FilterProvincielte(i *Item, s string) bool {
	return Provincie.GetValue(i.Provincie) <= s
}

// getter Provincie
func GettersProvincie(i *Item) string {
	return Provincie.GetValue(i.Provincie)
}

// contain filter Gemeente
func FilterGemeenteContains(i *Item, s string) bool {
	return strings.Contains(i.Gemeente, s)
}

// startswith filter Gemeente
func FilterGemeenteStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Gemeente, s)
}

// match filters Gemeente
func FilterGemeenteMatch(i *Item, s string) bool {
	return i.Gemeente == s
}

// gte filters Gemeente
func FilterGemeentegte(i *Item, s string) bool {
	return i.Gemeente <= s
}

// lte filters Gemeente
func FilterGemeentelte(i *Item, s string) bool {
	return i.Gemeente <= s
}

// getter Gemeente
func GettersGemeente(i *Item) string {
	return i.Gemeente
}

// contain filter Wijk
func FilterWijkContains(i *Item, s string) bool {
	return strings.Contains(i.Wijk, s)
}

// startswith filter Wijk
func FilterWijkStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Wijk, s)
}

// match filters Wijk
func FilterWijkMatch(i *Item, s string) bool {
	return i.Wijk == s
}

// gte filters Wijk
func FilterWijkgte(i *Item, s string) bool {
	return i.Wijk <= s
}

// lte filters Wijk
func FilterWijklte(i *Item, s string) bool {
	return i.Wijk <= s
}

// getter Wijk
func GettersWijk(i *Item) string {
	return i.Wijk
}

// contain filter Buurt
func FilterBuurtContains(i *Item, s string) bool {
	return strings.Contains(i.Buurt, s)
}

// startswith filter Buurt
func FilterBuurtStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Buurt, s)
}

// match filters Buurt
func FilterBuurtMatch(i *Item, s string) bool {
	return i.Buurt == s
}

// gte filters Buurt
func FilterBuurtgte(i *Item, s string) bool {
	return i.Buurt <= s
}

// lte filters Buurt
func FilterBuurtlte(i *Item, s string) bool {
	return i.Buurt <= s
}

// getter Buurt
func GettersBuurt(i *Item) string {
	return i.Buurt
}

// contain filter Regiocode
func FilterRegiocodeContains(i *Item, s string) bool {
	return strings.Contains(i.Regiocode, s)
}

// startswith filter Regiocode
func FilterRegiocodeStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Regiocode, s)
}

// match filters Regiocode
func FilterRegiocodeMatch(i *Item, s string) bool {
	return i.Regiocode == s
}

// gte filters Regiocode
func FilterRegiocodegte(i *Item, s string) bool {
	return i.Regiocode <= s
}

// lte filters Regiocode
func FilterRegiocodelte(i *Item, s string) bool {
	return i.Regiocode <= s
}

// getter Regiocode
func GettersRegiocode(i *Item) string {
	return i.Regiocode
}

// contain filter SoortEigendomWoning
func FilterSoortEigendomWoningContains(i *Item, s string) bool {
	return strings.Contains(i.SoortEigendomWoning, s)
}

// startswith filter SoortEigendomWoning
func FilterSoortEigendomWoningStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.SoortEigendomWoning, s)
}

// match filters SoortEigendomWoning
func FilterSoortEigendomWoningMatch(i *Item, s string) bool {
	return i.SoortEigendomWoning == s
}

// gte filters SoortEigendomWoning
func FilterSoortEigendomWoninggte(i *Item, s string) bool {
	return i.SoortEigendomWoning <= s
}

// lte filters SoortEigendomWoning
func FilterSoortEigendomWoninglte(i *Item, s string) bool {
	return i.SoortEigendomWoning <= s
}

// getter SoortEigendomWoning
func GettersSoortEigendomWoning(i *Item) string {
	return i.SoortEigendomWoning
}

// contain filter AantalHuishoudens1
func FilterAantalHuishoudens1Contains(i *Item, s string) bool {
	return strings.Contains(i.AantalHuishoudens1, s)
}

// startswith filter AantalHuishoudens1
func FilterAantalHuishoudens1StartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.AantalHuishoudens1, s)
}

// match filters AantalHuishoudens1
func FilterAantalHuishoudens1Match(i *Item, s string) bool {
	return i.AantalHuishoudens1 == s
}

// gte filters AantalHuishoudens1
func FilterAantalHuishoudens1gte(i *Item, s string) bool {
	return i.AantalHuishoudens1 <= s
}

// lte filters AantalHuishoudens1
func FilterAantalHuishoudens1lte(i *Item, s string) bool {
	return i.AantalHuishoudens1 <= s
}

// getter AantalHuishoudens1
func GettersAantalHuishoudens1(i *Item) string {
	return i.AantalHuishoudens1
}

// contain filter EnergiearmoedeLihelek2
func FilterEnergiearmoedeLihelek2Contains(i *Item, s string) bool {
	return strings.Contains(i.EnergiearmoedeLihelek2, s)
}

// startswith filter EnergiearmoedeLihelek2
func FilterEnergiearmoedeLihelek2StartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.EnergiearmoedeLihelek2, s)
}

// match filters EnergiearmoedeLihelek2
func FilterEnergiearmoedeLihelek2Match(i *Item, s string) bool {
	return i.EnergiearmoedeLihelek2 == s
}

// gte filters EnergiearmoedeLihelek2
func FilterEnergiearmoedeLihelek2gte(i *Item, s string) bool {
	return i.EnergiearmoedeLihelek2 <= s
}

// lte filters EnergiearmoedeLihelek2
func FilterEnergiearmoedeLihelek2lte(i *Item, s string) bool {
	return i.EnergiearmoedeLihelek2 <= s
}

// getter EnergiearmoedeLihelek2
func GettersEnergiearmoedeLihelek2(i *Item) string {
	return i.EnergiearmoedeLihelek2
}

// contain filter EnergiearmoedeHeq
func FilterEnergiearmoedeHeqContains(i *Item, s string) bool {
	return strings.Contains(i.EnergiearmoedeHeq, s)
}

// startswith filter EnergiearmoedeHeq
func FilterEnergiearmoedeHeqStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.EnergiearmoedeHeq, s)
}

// match filters EnergiearmoedeHeq
func FilterEnergiearmoedeHeqMatch(i *Item, s string) bool {
	return i.EnergiearmoedeHeq == s
}

// gte filters EnergiearmoedeHeq
func FilterEnergiearmoedeHeqgte(i *Item, s string) bool {
	return i.EnergiearmoedeHeq <= s
}

// lte filters EnergiearmoedeHeq
func FilterEnergiearmoedeHeqlte(i *Item, s string) bool {
	return i.EnergiearmoedeHeq <= s
}

// getter EnergiearmoedeHeq
func GettersEnergiearmoedeHeq(i *Item) string {
	return i.EnergiearmoedeHeq
}

// contain filter EnergiearmoedeLihe
func FilterEnergiearmoedeLiheContains(i *Item, s string) bool {
	return strings.Contains(i.EnergiearmoedeLihe, s)
}

// startswith filter EnergiearmoedeLihe
func FilterEnergiearmoedeLiheStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.EnergiearmoedeLihe, s)
}

// match filters EnergiearmoedeLihe
func FilterEnergiearmoedeLiheMatch(i *Item, s string) bool {
	return i.EnergiearmoedeLihe == s
}

// gte filters EnergiearmoedeLihe
func FilterEnergiearmoedeLihegte(i *Item, s string) bool {
	return i.EnergiearmoedeLihe <= s
}

// lte filters EnergiearmoedeLihe
func FilterEnergiearmoedeLihelte(i *Item, s string) bool {
	return i.EnergiearmoedeLihe <= s
}

// getter EnergiearmoedeLihe
func GettersEnergiearmoedeLihe(i *Item) string {
	return i.EnergiearmoedeLihe
}

// contain filter EnergiearmoedeLilek
func FilterEnergiearmoedeLilekContains(i *Item, s string) bool {
	return strings.Contains(i.EnergiearmoedeLilek, s)
}

// startswith filter EnergiearmoedeLilek
func FilterEnergiearmoedeLilekStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.EnergiearmoedeLilek, s)
}

// match filters EnergiearmoedeLilek
func FilterEnergiearmoedeLilekMatch(i *Item, s string) bool {
	return i.EnergiearmoedeLilek == s
}

// gte filters EnergiearmoedeLilek
func FilterEnergiearmoedeLilekgte(i *Item, s string) bool {
	return i.EnergiearmoedeLilek <= s
}

// lte filters EnergiearmoedeLilek
func FilterEnergiearmoedeLileklte(i *Item, s string) bool {
	return i.EnergiearmoedeLilek <= s
}

// getter EnergiearmoedeLilek
func GettersEnergiearmoedeLilek(i *Item) string {
	return i.EnergiearmoedeLilek
}

// contain filter EnergiearmoedeLekwi
func FilterEnergiearmoedeLekwiContains(i *Item, s string) bool {
	return strings.Contains(i.EnergiearmoedeLekwi, s)
}

// startswith filter EnergiearmoedeLekwi
func FilterEnergiearmoedeLekwiStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.EnergiearmoedeLekwi, s)
}

// match filters EnergiearmoedeLekwi
func FilterEnergiearmoedeLekwiMatch(i *Item, s string) bool {
	return i.EnergiearmoedeLekwi == s
}

// gte filters EnergiearmoedeLekwi
func FilterEnergiearmoedeLekwigte(i *Item, s string) bool {
	return i.EnergiearmoedeLekwi <= s
}

// lte filters EnergiearmoedeLekwi
func FilterEnergiearmoedeLekwilte(i *Item, s string) bool {
	return i.EnergiearmoedeLekwi <= s
}

// getter EnergiearmoedeLekwi
func GettersEnergiearmoedeLekwi(i *Item) string {
	return i.EnergiearmoedeLekwi
}

// contain filter ResidueelInkomen
func FilterResidueelInkomenContains(i *Item, s string) bool {
	return strings.Contains(i.ResidueelInkomen, s)
}

// startswith filter ResidueelInkomen
func FilterResidueelInkomenStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.ResidueelInkomen, s)
}

// match filters ResidueelInkomen
func FilterResidueelInkomenMatch(i *Item, s string) bool {
	return i.ResidueelInkomen == s
}

// gte filters ResidueelInkomen
func FilterResidueelInkomengte(i *Item, s string) bool {
	return i.ResidueelInkomen <= s
}

// lte filters ResidueelInkomen
func FilterResidueelInkomenlte(i *Item, s string) bool {
	return i.ResidueelInkomen <= s
}

// getter ResidueelInkomen
func GettersResidueelInkomen(i *Item) string {
	return i.ResidueelInkomen
}

// contain filter Energiequote
func FilterEnergiequoteContains(i *Item, s string) bool {
	return strings.Contains(i.Energiequote, s)
}

// startswith filter Energiequote
func FilterEnergiequoteStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Energiequote, s)
}

// match filters Energiequote
func FilterEnergiequoteMatch(i *Item, s string) bool {
	return i.Energiequote == s
}

// gte filters Energiequote
func FilterEnergiequotegte(i *Item, s string) bool {
	return i.Energiequote <= s
}

// lte filters Energiequote
func FilterEnergiequotelte(i *Item, s string) bool {
	return i.Energiequote <= s
}

// getter Energiequote
func GettersEnergiequote(i *Item) string {
	return i.Energiequote
}

// contain filter Buurtcode
func FilterBuurtcodeContains(i *Item, s string) bool {
	return strings.Contains(Buurtcode.GetValue(i.Buurtcode), s)
}

// startswith filter Buurtcode
func FilterBuurtcodeStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Buurtcode.GetValue(i.Buurtcode), s)
}

// match filters Buurtcode
func FilterBuurtcodeMatch(i *Item, s string) bool {
	return Buurtcode.GetValue(i.Buurtcode) == s
}

// gte filters Buurtcode
func FilterBuurtcodegte(i *Item, s string) bool {
	return Buurtcode.GetValue(i.Buurtcode) <= s
}

// lte filters Buurtcode
func FilterBuurtcodelte(i *Item, s string) bool {
	return Buurtcode.GetValue(i.Buurtcode) <= s
}

// getter Buurtcode
func GettersBuurtcode(i *Item) string {
	return Buurtcode.GetValue(i.Buurtcode)
}

// contain filter Wijkcode
func FilterWijkcodeContains(i *Item, s string) bool {
	return strings.Contains(Wijkcode.GetValue(i.Wijkcode), s)
}

// startswith filter Wijkcode
func FilterWijkcodeStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Wijkcode.GetValue(i.Wijkcode), s)
}

// match filters Wijkcode
func FilterWijkcodeMatch(i *Item, s string) bool {
	return Wijkcode.GetValue(i.Wijkcode) == s
}

// gte filters Wijkcode
func FilterWijkcodegte(i *Item, s string) bool {
	return Wijkcode.GetValue(i.Wijkcode) <= s
}

// lte filters Wijkcode
func FilterWijkcodelte(i *Item, s string) bool {
	return Wijkcode.GetValue(i.Wijkcode) <= s
}

// getter Wijkcode
func GettersWijkcode(i *Item) string {
	return Wijkcode.GetValue(i.Wijkcode)
}

// contain filter Gemeentecode
func FilterGemeentecodeContains(i *Item, s string) bool {
	return strings.Contains(Gemeentecode.GetValue(i.Gemeentecode), s)
}

// startswith filter Gemeentecode
func FilterGemeentecodeStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Gemeentecode.GetValue(i.Gemeentecode), s)
}

// match filters Gemeentecode
func FilterGemeentecodeMatch(i *Item, s string) bool {
	return Gemeentecode.GetValue(i.Gemeentecode) == s
}

// gte filters Gemeentecode
func FilterGemeentecodegte(i *Item, s string) bool {
	return Gemeentecode.GetValue(i.Gemeentecode) <= s
}

// lte filters Gemeentecode
func FilterGemeentecodelte(i *Item, s string) bool {
	return Gemeentecode.GetValue(i.Gemeentecode) <= s
}

// getter Gemeentecode
func GettersGemeentecode(i *Item) string {
	return Gemeentecode.GetValue(i.Gemeentecode)
}

/*
// contain filters
func FilterEkeyContains(i *Item, s string) bool {
	return strings.Contains(i.Ekey, s)
}


// startswith filters
func FilterEkeyStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Ekey, s)
}


// match filters
func FilterEkeyMatch(i *Item, s string) bool {
	return i.Ekey == s
}

// getters
func GettersEkey(i *Item) string {
	return i.Ekey
}
*/

// reduce functions
func reduceCount(items Items) map[string]string {
	result := make(map[string]string)
	result["count"] = strconv.Itoa(len(items))
	return result
}

type GroupedOperations struct {
	Funcs     registerFuncType
	GroupBy   registerGroupByFunc
	Getters   registerGettersMap
	Reduce    registerReduce
	BitArrays registerBitArray
}

var Operations GroupedOperations

var RegisterFuncMap registerFuncType
var RegisterGroupBy registerGroupByFunc
var RegisterGetters registerGettersMap
var RegisterReduce registerReduce
var RegisterBitArray registerBitArray

// ValidateRegsiters validate exposed columns do match filter names
func validateRegisters() {
	var i = ItemOut{}
	var filters = []string{"match", "contains", "startswith"}
	for _, c := range i.Columns() {
		for _, f := range filters {
			filterKey := f + "-" + c
			if _, ok := RegisterFuncMap[filterKey]; !ok {
				fmt.Println("missing in RegisterMap: " + filterKey)
			}
		}
	}
}

func FilterGeneralStartsWith(i *Item, s string) bool {
	return FilterNrStartsWith(i, s) ||
		FilterProvincieStartsWith(i, s) ||
		FilterGemeenteStartsWith(i, s) ||
		FilterWijkStartsWith(i, s) ||
		FilterBuurtStartsWith(i, s) ||
		FilterRegiocodeStartsWith(i, s) ||
		FilterSoortEigendomWoningStartsWith(i, s) ||
		FilterAantalHuishoudens1StartsWith(i, s) ||
		FilterEnergiearmoedeLihelek2StartsWith(i, s) ||
		FilterEnergiearmoedeHeqStartsWith(i, s) ||
		FilterEnergiearmoedeLiheStartsWith(i, s) ||
		FilterEnergiearmoedeLilekStartsWith(i, s) ||
		FilterEnergiearmoedeLekwiStartsWith(i, s) ||
		FilterResidueelInkomenStartsWith(i, s) ||
		FilterEnergiequoteStartsWith(i, s) ||
		FilterBuurtcodeStartsWith(i, s) ||
		FilterWijkcodeStartsWith(i, s) ||
		FilterGemeentecodeStartsWith(i, s)
}

func init() {

	RegisterFuncMap = make(registerFuncType)
	RegisterGroupBy = make(registerGroupByFunc)
	RegisterGetters = make(registerGettersMap)
	RegisterReduce = make(registerReduce)

	// register search filter.
	//RegisterFuncMap["search"] = 'EDITYOURSELF'
	RegisterFuncMap["search"] = FilterGeneralStartsWith

	//RegisterFuncMap["value"] = 'EDITYOURSELF'
	// example RegisterGetters["value"] = GettersEkey

	// register filters

	//register filters for Nr
	RegisterFuncMap["match-nr"] = FilterNrMatch
	RegisterFuncMap["contains-nr"] = FilterNrContains
	RegisterFuncMap["startswith-nr"] = FilterNrStartsWith
	RegisterFuncMap["gte-nr"] = FilterNrgte
	RegisterFuncMap["lte-nr"] = FilterNrlte
	RegisterGetters["nr"] = GettersNr
	RegisterGroupBy["nr"] = GettersNr

	//register filters for Provincie
	RegisterFuncMap["match-provincie"] = FilterProvincieMatch
	RegisterFuncMap["contains-provincie"] = FilterProvincieContains
	RegisterFuncMap["startswith-provincie"] = FilterProvincieStartsWith
	RegisterFuncMap["gte-provincie"] = FilterProvinciegte
	RegisterFuncMap["lte-provincie"] = FilterProvincielte
	RegisterGetters["provincie"] = GettersProvincie
	RegisterGroupBy["provincie"] = GettersProvincie

	//register filters for Gemeente
	RegisterFuncMap["match-gemeente"] = FilterGemeenteMatch
	RegisterFuncMap["contains-gemeente"] = FilterGemeenteContains
	RegisterFuncMap["startswith-gemeente"] = FilterGemeenteStartsWith
	RegisterFuncMap["gte-gemeente"] = FilterGemeentegte
	RegisterFuncMap["lte-gemeente"] = FilterGemeentelte
	RegisterGetters["gemeente"] = GettersGemeente
	RegisterGroupBy["gemeente"] = GettersGemeente

	//register filters for Wijk
	RegisterFuncMap["match-wijk"] = FilterWijkMatch
	RegisterFuncMap["contains-wijk"] = FilterWijkContains
	RegisterFuncMap["startswith-wijk"] = FilterWijkStartsWith
	RegisterFuncMap["gte-wijk"] = FilterWijkgte
	RegisterFuncMap["lte-wijk"] = FilterWijklte
	RegisterGetters["wijk"] = GettersWijk
	RegisterGroupBy["wijk"] = GettersWijk

	//register filters for Buurt
	RegisterFuncMap["match-buurt"] = FilterBuurtMatch
	RegisterFuncMap["contains-buurt"] = FilterBuurtContains
	RegisterFuncMap["startswith-buurt"] = FilterBuurtStartsWith
	RegisterFuncMap["gte-buurt"] = FilterBuurtgte
	RegisterFuncMap["lte-buurt"] = FilterBuurtlte
	RegisterGetters["buurt"] = GettersBuurt
	RegisterGroupBy["buurt"] = GettersBuurt

	//register filters for Regiocode
	RegisterFuncMap["match-regiocode"] = FilterRegiocodeMatch
	RegisterFuncMap["contains-regiocode"] = FilterRegiocodeContains
	RegisterFuncMap["startswith-regiocode"] = FilterRegiocodeStartsWith
	RegisterFuncMap["gte-regiocode"] = FilterRegiocodegte
	RegisterFuncMap["lte-regiocode"] = FilterRegiocodelte
	RegisterGetters["regiocode"] = GettersRegiocode
	RegisterGroupBy["regiocode"] = GettersRegiocode

	//register filters for SoortEigendomWoning
	RegisterFuncMap["match-soort_eigendom_woning"] = FilterSoortEigendomWoningMatch
	RegisterFuncMap["contains-soort_eigendom_woning"] = FilterSoortEigendomWoningContains
	RegisterFuncMap["startswith-soort_eigendom_woning"] = FilterSoortEigendomWoningStartsWith
	RegisterFuncMap["gte-soort_eigendom_woning"] = FilterSoortEigendomWoninggte
	RegisterFuncMap["lte-soort_eigendom_woning"] = FilterSoortEigendomWoninglte
	RegisterGetters["soort_eigendom_woning"] = GettersSoortEigendomWoning
	RegisterGroupBy["soort_eigendom_woning"] = GettersSoortEigendomWoning

	//register filters for AantalHuishoudens1
	RegisterFuncMap["match-aantal_huishoudens1"] = FilterAantalHuishoudens1Match
	RegisterFuncMap["contains-aantal_huishoudens1"] = FilterAantalHuishoudens1Contains
	RegisterFuncMap["startswith-aantal_huishoudens1"] = FilterAantalHuishoudens1StartsWith
	RegisterFuncMap["gte-aantal_huishoudens1"] = FilterAantalHuishoudens1gte
	RegisterFuncMap["lte-aantal_huishoudens1"] = FilterAantalHuishoudens1lte
	RegisterGetters["aantal_huishoudens1"] = GettersAantalHuishoudens1
	RegisterGroupBy["aantal_huishoudens1"] = GettersAantalHuishoudens1

	//register filters for EnergiearmoedeLihelek2
	RegisterFuncMap["match-energiearmoede_lihelek2"] = FilterEnergiearmoedeLihelek2Match
	RegisterFuncMap["contains-energiearmoede_lihelek2"] = FilterEnergiearmoedeLihelek2Contains
	RegisterFuncMap["startswith-energiearmoede_lihelek2"] = FilterEnergiearmoedeLihelek2StartsWith
	RegisterFuncMap["gte-energiearmoede_lihelek2"] = FilterEnergiearmoedeLihelek2gte
	RegisterFuncMap["lte-energiearmoede_lihelek2"] = FilterEnergiearmoedeLihelek2lte
	RegisterGetters["energiearmoede_lihelek2"] = GettersEnergiearmoedeLihelek2
	RegisterGroupBy["energiearmoede_lihelek2"] = GettersEnergiearmoedeLihelek2

	//register filters for EnergiearmoedeHeq
	RegisterFuncMap["match-energiearmoede_heq"] = FilterEnergiearmoedeHeqMatch
	RegisterFuncMap["contains-energiearmoede_heq"] = FilterEnergiearmoedeHeqContains
	RegisterFuncMap["startswith-energiearmoede_heq"] = FilterEnergiearmoedeHeqStartsWith
	RegisterFuncMap["gte-energiearmoede_heq"] = FilterEnergiearmoedeHeqgte
	RegisterFuncMap["lte-energiearmoede_heq"] = FilterEnergiearmoedeHeqlte
	RegisterGetters["energiearmoede_heq"] = GettersEnergiearmoedeHeq
	RegisterGroupBy["energiearmoede_heq"] = GettersEnergiearmoedeHeq

	//register filters for EnergiearmoedeLihe
	RegisterFuncMap["match-energiearmoede_lihe"] = FilterEnergiearmoedeLiheMatch
	RegisterFuncMap["contains-energiearmoede_lihe"] = FilterEnergiearmoedeLiheContains
	RegisterFuncMap["startswith-energiearmoede_lihe"] = FilterEnergiearmoedeLiheStartsWith
	RegisterFuncMap["gte-energiearmoede_lihe"] = FilterEnergiearmoedeLihegte
	RegisterFuncMap["lte-energiearmoede_lihe"] = FilterEnergiearmoedeLihelte
	RegisterGetters["energiearmoede_lihe"] = GettersEnergiearmoedeLihe
	RegisterGroupBy["energiearmoede_lihe"] = GettersEnergiearmoedeLihe

	//register filters for EnergiearmoedeLilek
	RegisterFuncMap["match-energiearmoede_lilek"] = FilterEnergiearmoedeLilekMatch
	RegisterFuncMap["contains-energiearmoede_lilek"] = FilterEnergiearmoedeLilekContains
	RegisterFuncMap["startswith-energiearmoede_lilek"] = FilterEnergiearmoedeLilekStartsWith
	RegisterFuncMap["gte-energiearmoede_lilek"] = FilterEnergiearmoedeLilekgte
	RegisterFuncMap["lte-energiearmoede_lilek"] = FilterEnergiearmoedeLileklte
	RegisterGetters["energiearmoede_lilek"] = GettersEnergiearmoedeLilek
	RegisterGroupBy["energiearmoede_lilek"] = GettersEnergiearmoedeLilek

	//register filters for EnergiearmoedeLekwi
	RegisterFuncMap["match-energiearmoede_lekwi"] = FilterEnergiearmoedeLekwiMatch
	RegisterFuncMap["contains-energiearmoede_lekwi"] = FilterEnergiearmoedeLekwiContains
	RegisterFuncMap["startswith-energiearmoede_lekwi"] = FilterEnergiearmoedeLekwiStartsWith
	RegisterFuncMap["gte-energiearmoede_lekwi"] = FilterEnergiearmoedeLekwigte
	RegisterFuncMap["lte-energiearmoede_lekwi"] = FilterEnergiearmoedeLekwilte
	RegisterGetters["energiearmoede_lekwi"] = GettersEnergiearmoedeLekwi
	RegisterGroupBy["energiearmoede_lekwi"] = GettersEnergiearmoedeLekwi

	//register filters for ResidueelInkomen
	RegisterFuncMap["match-residueel_inkomen"] = FilterResidueelInkomenMatch
	RegisterFuncMap["contains-residueel_inkomen"] = FilterResidueelInkomenContains
	RegisterFuncMap["startswith-residueel_inkomen"] = FilterResidueelInkomenStartsWith
	RegisterFuncMap["gte-residueel_inkomen"] = FilterResidueelInkomengte
	RegisterFuncMap["lte-residueel_inkomen"] = FilterResidueelInkomenlte
	RegisterGetters["residueel_inkomen"] = GettersResidueelInkomen
	RegisterGroupBy["residueel_inkomen"] = GettersResidueelInkomen

	//register filters for Energiequote
	RegisterFuncMap["match-energiequote"] = FilterEnergiequoteMatch
	RegisterFuncMap["contains-energiequote"] = FilterEnergiequoteContains
	RegisterFuncMap["startswith-energiequote"] = FilterEnergiequoteStartsWith
	RegisterFuncMap["gte-energiequote"] = FilterEnergiequotegte
	RegisterFuncMap["lte-energiequote"] = FilterEnergiequotelte
	RegisterGetters["energiequote"] = GettersEnergiequote
	RegisterGroupBy["energiequote"] = GettersEnergiequote

	//register filters for Buurtcode
	RegisterFuncMap["match-buurtcode"] = FilterBuurtcodeMatch
	RegisterFuncMap["contains-buurtcode"] = FilterBuurtcodeContains
	RegisterFuncMap["startswith-buurtcode"] = FilterBuurtcodeStartsWith
	RegisterFuncMap["gte-buurtcode"] = FilterBuurtcodegte
	RegisterFuncMap["lte-buurtcode"] = FilterBuurtcodelte
	RegisterGetters["buurtcode"] = GettersBuurtcode
	RegisterGroupBy["buurtcode"] = GettersBuurtcode

	//register filters for Wijkcode
	RegisterFuncMap["match-wijkcode"] = FilterWijkcodeMatch
	RegisterFuncMap["contains-wijkcode"] = FilterWijkcodeContains
	RegisterFuncMap["startswith-wijkcode"] = FilterWijkcodeStartsWith
	RegisterFuncMap["gte-wijkcode"] = FilterWijkcodegte
	RegisterFuncMap["lte-wijkcode"] = FilterWijkcodelte
	RegisterGetters["wijkcode"] = GettersWijkcode
	RegisterGroupBy["wijkcode"] = GettersWijkcode

	//register filters for Gemeentecode
	RegisterFuncMap["match-gemeentecode"] = FilterGemeentecodeMatch
	RegisterFuncMap["contains-gemeentecode"] = FilterGemeentecodeContains
	RegisterFuncMap["startswith-gemeentecode"] = FilterGemeentecodeStartsWith
	RegisterFuncMap["gte-gemeentecode"] = FilterGemeentecodegte
	RegisterFuncMap["lte-gemeentecode"] = FilterGemeentecodelte
	RegisterGetters["gemeentecode"] = GettersGemeentecode
	RegisterGroupBy["gemeentecode"] = GettersGemeentecode

	validateRegisters()

	/*
		RegisterFuncMap["match-ekey"] = FilterEkeyMatch
		RegisterFuncMap["contains-ekey"] = FilterEkeyContains
		// register startswith filters
		RegisterFuncMap["startswith-ekey"] = FilterEkeyStartsWith
		// register getters
		RegisterGetters["ekey"] = GettersEkey
		// register groupby
		RegisterGroupBy["ekey"] = GettersEkey

	*/

	// register reduce functions
	RegisterReduce["count"] = reduceCount

	// custom
	// RegisterReduce["woningequivalent"] = reduceWEQ
	// RegisterReduce["eancodes"] = reduceEAN

}

type sortLookup map[string]func(int, int) bool

func createSort(items Items) sortLookup {

	sortFuncs := sortLookup{

		"nr":  func(i, j int) bool { return items[i].Nr < items[j].Nr },
		"-nr": func(i, j int) bool { return items[i].Nr > items[j].Nr },

		"provincie": func(i, j int) bool {
			return Provincie.GetValue(items[i].Provincie) < Provincie.GetValue(items[j].Provincie)
		},
		"-provincie": func(i, j int) bool {
			return Provincie.GetValue(items[i].Provincie) > Provincie.GetValue(items[j].Provincie)
		},

		"gemeente":  func(i, j int) bool { return items[i].Gemeente < items[j].Gemeente },
		"-gemeente": func(i, j int) bool { return items[i].Gemeente > items[j].Gemeente },

		"wijk":  func(i, j int) bool { return items[i].Wijk < items[j].Wijk },
		"-wijk": func(i, j int) bool { return items[i].Wijk > items[j].Wijk },

		"buurt":  func(i, j int) bool { return items[i].Buurt < items[j].Buurt },
		"-buurt": func(i, j int) bool { return items[i].Buurt > items[j].Buurt },

		"regiocode":  func(i, j int) bool { return items[i].Regiocode < items[j].Regiocode },
		"-regiocode": func(i, j int) bool { return items[i].Regiocode > items[j].Regiocode },

		"soort_eigendom_woning":  func(i, j int) bool { return items[i].SoortEigendomWoning < items[j].SoortEigendomWoning },
		"-soort_eigendom_woning": func(i, j int) bool { return items[i].SoortEigendomWoning > items[j].SoortEigendomWoning },

		"aantal_huishoudens1":  func(i, j int) bool { return items[i].AantalHuishoudens1 < items[j].AantalHuishoudens1 },
		"-aantal_huishoudens1": func(i, j int) bool { return items[i].AantalHuishoudens1 > items[j].AantalHuishoudens1 },

		"energiearmoede_lihelek2":  func(i, j int) bool { return items[i].EnergiearmoedeLihelek2 < items[j].EnergiearmoedeLihelek2 },
		"-energiearmoede_lihelek2": func(i, j int) bool { return items[i].EnergiearmoedeLihelek2 > items[j].EnergiearmoedeLihelek2 },

		"energiearmoede_heq":  func(i, j int) bool { return items[i].EnergiearmoedeHeq < items[j].EnergiearmoedeHeq },
		"-energiearmoede_heq": func(i, j int) bool { return items[i].EnergiearmoedeHeq > items[j].EnergiearmoedeHeq },

		"energiearmoede_lihe":  func(i, j int) bool { return items[i].EnergiearmoedeLihe < items[j].EnergiearmoedeLihe },
		"-energiearmoede_lihe": func(i, j int) bool { return items[i].EnergiearmoedeLihe > items[j].EnergiearmoedeLihe },

		"energiearmoede_lilek":  func(i, j int) bool { return items[i].EnergiearmoedeLilek < items[j].EnergiearmoedeLilek },
		"-energiearmoede_lilek": func(i, j int) bool { return items[i].EnergiearmoedeLilek > items[j].EnergiearmoedeLilek },

		"energiearmoede_lekwi":  func(i, j int) bool { return items[i].EnergiearmoedeLekwi < items[j].EnergiearmoedeLekwi },
		"-energiearmoede_lekwi": func(i, j int) bool { return items[i].EnergiearmoedeLekwi > items[j].EnergiearmoedeLekwi },

		"residueel_inkomen":  func(i, j int) bool { return items[i].ResidueelInkomen < items[j].ResidueelInkomen },
		"-residueel_inkomen": func(i, j int) bool { return items[i].ResidueelInkomen > items[j].ResidueelInkomen },

		"energiequote":  func(i, j int) bool { return items[i].Energiequote < items[j].Energiequote },
		"-energiequote": func(i, j int) bool { return items[i].Energiequote > items[j].Energiequote },

		"buurtcode": func(i, j int) bool {
			return Buurtcode.GetValue(items[i].Buurtcode) < Buurtcode.GetValue(items[j].Buurtcode)
		},
		"-buurtcode": func(i, j int) bool {
			return Buurtcode.GetValue(items[i].Buurtcode) > Buurtcode.GetValue(items[j].Buurtcode)
		},

		"wijkcode": func(i, j int) bool {
			return Wijkcode.GetValue(items[i].Wijkcode) < Wijkcode.GetValue(items[j].Wijkcode)
		},
		"-wijkcode": func(i, j int) bool {
			return Wijkcode.GetValue(items[i].Wijkcode) > Wijkcode.GetValue(items[j].Wijkcode)
		},

		"gemeentecode": func(i, j int) bool {
			return Gemeentecode.GetValue(items[i].Gemeentecode) < Gemeentecode.GetValue(items[j].Gemeentecode)
		},
		"-gemeentecode": func(i, j int) bool {
			return Gemeentecode.GetValue(items[i].Gemeentecode) > Gemeentecode.GetValue(items[j].Gemeentecode)
		},
	}
	return sortFuncs
}

func sortBy(items Items, sortingL []string) (Items, []string) {
	sortFuncs := createSort(items)

	for _, sortFuncName := range sortingL {
		sortFunc, ok := sortFuncs[sortFuncName]
		if ok {
			sort.Slice(items, sortFunc)
		}
	}

	// TODO must be nicer way
	keys := []string{}
	for key := range sortFuncs {
		keys = append(keys, key)
	}

	return items, keys
}
